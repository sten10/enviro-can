/*
 * can.h
 *
 *  Created on: Nov 30, 2021
 *      Author: sten
 */

#ifndef INC_CAN_H_
#define INC_CAN_H_

union canData{
	uint8_t byte[8];
	uint32_t uint32[2];
};

typedef struct can_frame_t{
	uint32_t id;
	union canData data[8];
	uint8_t dlc;
}can_frame_t;

int can_init(CAN_HandleTypeDef* _canHandle);
int can_send(can_frame_t* canFrame);
int can_read(can_frame_t* canFrame);

#endif /* INC_CAN_H_ */
