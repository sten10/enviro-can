/*
 * led.h
 *
 *  Created on: Jan 9, 2022
 *      Author: sten
 */

#ifndef LED_H_
#define LED_H_

enum led_name_e{
	LED_GREEN,
	LED_RED
};

void led_init();
void led_task();
void led_set(enum led_name_e led, uint32_t value);
void led_greenPulse();

#endif /* LED_H_ */
