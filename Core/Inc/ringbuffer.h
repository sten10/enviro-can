/*
 * ringbuffer.h
 *
 *  Created on: Nov 21, 2021
 *      Author: sten
 */

#ifndef INC_RINGBUFFER_H_
#define INC_RINGBUFFER_H_

#include <stdint.h>

typedef struct ringbuffer_t{
	uint8_t* base;
	uint8_t* top;
	volatile uint8_t* wrPtr;
	volatile uint8_t* rdPtr;
} ringbuffer_t;

void ringbuffer_init();
int ringbuffer_writeByte(ringbuffer_t* ringBuffer, uint8_t byte);
int ringbuffer_write(ringbuffer_t* ringBuffer, uint8_t* dataSrc,  size_t len);
int ringbuffer_read(ringbuffer_t* ringBuffer, uint8_t* dataDest,  size_t maxLen);

#endif /* INC_RINGBUFFER_H_ */
