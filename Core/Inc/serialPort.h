/*
 * serialPort.h
 *
 *  Created on: Nov 14, 2021
 *      Author: sten
 */

#ifndef INC_SERIALPORT_H_
#define INC_SERIALPORT_H_

#include "stm32f0xx_hal.h"

int serialPort_init(UART_HandleTypeDef* uartHandle);
int serialPort_writeString(char* str);
int serialPort_writeLine(char* str);
int serialPort_read(uint8_t *dataDest, size_t maxLen);

#endif /* INC_SERIALPORT_H_ */
