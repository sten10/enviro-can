/*
 * status.h
 *
 *  Created on: Jan 4, 2022
 *      Author: sten
 */

#ifndef INC_STATUS_H_
#define INC_STATUS_H_

#include <stdint.h>

enum status_maskBit{
	CAN_WRITE_FAILED = 8,
};

void status_setBit(uint8_t statusBit);
uint32_t status_get();

#endif /* INC_STATUS_H_ */
