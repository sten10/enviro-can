/*
 * sysloop.h
 *
 *  Created on: Nov 14, 2021
 *      Author: sten
 */

#ifndef INC_SYSLOOP_H_
#define INC_SYSLOOP_H_

void sysloop_init();
void sysloop_task();

#endif /* INC_SYSLOOP_H_ */
