/*
 * can.c
 *
 *  Created on: Nov 30, 2021
 *      Author: sten
 */

#include "stm32f0xx_hal.h"
#include "can.h"
#include "ringbuffer.h"
#include "status.h"
#include "stm32f0xx_hal.h"
#include "led.h"

#define CAN_TX_QUEUE_LEN	8

static can_frame_t rxBuffer[CAN_TX_QUEUE_LEN];
static ringbuffer_t rxRingBuffer;
static CAN_HandleTypeDef* canHandle;

static int sendFrame(can_frame_t* canFrame){
	uint32_t mailbox;
	CAN_TxHeaderTypeDef header = {
			.StdId = 0,
			.ExtId = canFrame->id,
			.IDE = CAN_ID_EXT,
			.RTR = CAN_RTR_DATA,
			.DLC = canFrame->dlc,
			.TransmitGlobalTime = DISABLE,
	};

	return HAL_CAN_AddTxMessage(canHandle, &header, canFrame->data->byte, &mailbox);
}

static void readCanFrame(CAN_HandleTypeDef *hcan, uint32_t rxFifoId){
	CAN_RxHeaderTypeDef header;
	can_frame_t canFrame;

	HAL_CAN_GetRxMessage(hcan, rxFifoId, &header, canFrame.data->byte);

	canFrame.id = header.ExtId;
	canFrame.dlc = header.DLC;

	if (ringbuffer_write(&rxRingBuffer, (uint8_t*)&canFrame, sizeof(can_frame_t)) < 0){
		status_setBit(CAN_WRITE_FAILED);
	}
}

static void setFilters(){
	CAN_FilterTypeDef filter;

	filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	filter.FilterIdHigh = 0;
	filter.FilterIdLow = 0;
	filter.FilterMaskIdHigh = 0;
	filter.FilterMaskIdLow = 0;
	filter.FilterScale = CAN_FILTERSCALE_32BIT;
	filter.FilterActivation = ENABLE;
	filter.FilterBank = 0;
	filter.FilterMode = CAN_FILTERMODE_IDMASK;
	filter.SlaveStartFilterBank = 14;
	HAL_CAN_ConfigFilter(canHandle, &filter);
}

int can_init(CAN_HandleTypeDef* _canHandle){
	CAN_RxHeaderTypeDef header;
	can_frame_t canFrame;

	ringbuffer_init(&rxRingBuffer, rxBuffer, sizeof(rxBuffer));

	canHandle = _canHandle;

	setFilters();
	HAL_CAN_Start(_canHandle);
	HAL_CAN_ActivateNotification(_canHandle, CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_RX_FIFO1_MSG_PENDING);
	HAL_CAN_GetRxMessage(_canHandle, 0, &header, canFrame.data->byte);

	return 0;
}

int can_send(can_frame_t* canFrame){

	sendFrame(canFrame);

	return 0;
}

int can_read(can_frame_t* canFrame){
	int rv = ringbuffer_read(&rxRingBuffer, (uint8_t*)canFrame, sizeof(can_frame_t));

	if (rv){
		led_greenPulse();
	}

	return rv;
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan){
	readCanFrame(hcan, 0);
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan){
	readCanFrame(hcan, 1);
}
