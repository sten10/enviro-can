/*
 * cmdHandler.c
 *
 *  Created on: Nov 22, 2021
 *      Author: sten
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "serialPort.h"
#include "can.h"
#include "log.h"
#include "status.h"

static void handleReceivedCanFrame(can_frame_t* canFrame){
	char buffer[28];

	snprintf(buffer, sizeof(buffer), "T%08lX%d", canFrame->id, canFrame->dlc);

	char* bufferWrPtr = buffer + 10;
	for(int i = 0; i < canFrame->dlc; i++){
		snprintf(bufferWrPtr, 3, "%02X", canFrame->data->byte[i]);
		bufferWrPtr += 2;
	}

	serialPort_writeLine(buffer);
}

static void handleReceivedSerialCommand(const uint8_t* cmd, size_t commandLen){
	uint8_t buffer[9];
	char* endptr;
	can_frame_t canFrame;

	if (cmd[0] == 'T'
			&& commandLen > 10
			&& commandLen < 28){

		// parse ID
		memcpy(buffer, cmd + 1, 8);
		buffer[8] = '\0';

		canFrame.id = strtoul((char*)buffer, &endptr, 16);
		if (endptr != (char*)(buffer + 8)){
			log_msg("failed to parse CAN ID");
			return;
		}

		// parse data length
		canFrame.dlc = cmd[9] - '0';
		if (canFrame.dlc > 8){
			log_msg("failed to parse CAN DLC");
			return;
		}

		if (canFrame.dlc > 8){
			serialPort_writeString("\7");
		}

		for (int i = 0; i < canFrame.dlc; i++){
			memcpy(buffer, cmd + 10 + (i * 2), 2);
			buffer[2] = '\0';

			canFrame.data->byte[i] = strtoul((char*)buffer, &endptr, 16);
			if (endptr != (char*)(buffer + 2)){
				log_msg("failed to parse CAN data");
				return;
			}
		}

		can_send(&canFrame);
	}
	else if (commandLen == 5
			&& strncmp((char*)cmd, "RESET", 5) == 0){
		NVIC_SystemReset();
	}
	else if (commandLen == 1){
		if (cmd[0] == 'F'){
			snprintf((char*)buffer, sizeof(buffer), "F%04lX", status_get());
			serialPort_writeLine((char*)buffer);
		}
	}
	else if (commandLen == 0){
		serialPort_writeLine("");
	}
	else{
		serialPort_writeString("\a");
	}
}

static void handleReceivedSerialDataByte(uint8_t receivedByte){
	static uint8_t cmdBuffer[64];
	static uint8_t* wrPtr = cmdBuffer;

	if (receivedByte == '\r'){
		handleReceivedSerialCommand(cmdBuffer, wrPtr - cmdBuffer);
		wrPtr = cmdBuffer;
	}
	else if (receivedByte != '\n'){
		if (wrPtr - cmdBuffer >= sizeof(cmdBuffer)){
			log_msg("receive command buffer overflow, discarding");
			wrPtr = cmdBuffer;
		}
		else{
			*wrPtr++ = receivedByte;
		}
	}
}

int cmdHandler_task(){
	int readCanFrameLen;
	int serialBytesRead;
	can_frame_t canFrame = {0};
	uint8_t dataByte;

	while((serialBytesRead = serialPort_read(&dataByte, 1)) > 0){
		handleReceivedSerialDataByte(dataByte);
	}

	while((readCanFrameLen = can_read(&canFrame)) > 0){
		handleReceivedCanFrame(&canFrame);
	}

	return 0;
}
