/*
 * led.c
 *
 *  Created on: Jan 9, 2022
 *      Author: sten
 */

#include "stm32f0xx_hal.h"
#include "led.h"

#define LED_PULSE_DURATION_IN_MS	70
#define LED_PULSE_DELAY_DURATION_IN_MS	(LED_PULSE_DURATION_IN_MS / 2)

enum ledState_e{
	LED_IDLE,
	LED_OFF,
	LED_ON_DELAY,
};

static uint32_t ledState;
static uint32_t countdown;

void led_init(){
	led_set(LED_GREEN, 1);
	led_set(LED_RED, 0);
}

void led_set(enum led_name_e led, uint32_t value){
	uint16_t gpioPin;

	if (led == LED_GREEN){
		gpioPin = GPIO_PIN_5;
	}
	else{
		gpioPin = GPIO_PIN_4;
	}

	HAL_GPIO_WritePin(GPIOA, gpioPin, value == 0);
}

void led_task(){

	switch(ledState){
	case LED_IDLE:
		break;

	case LED_OFF:
		if (countdown > 0){
			countdown--;
		}
		else{
			led_set(LED_GREEN, 1);
			countdown = LED_PULSE_DELAY_DURATION_IN_MS;
			ledState = LED_ON_DELAY;
		}
		break;

	case LED_ON_DELAY:
		if (countdown > 0){
			countdown--;
		}
		else{
			ledState = LED_IDLE;
		}
		break;
	}
}

void led_greenPulse(){
	if (ledState == LED_IDLE){
		countdown = LED_PULSE_DURATION_IN_MS;
		ledState = LED_OFF;
		led_set(LED_GREEN, 0);
	}
}
