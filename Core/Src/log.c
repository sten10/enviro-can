/*
 * log.c
 *
 *  Created on: Nov 14, 2021
 *      Author: sten
 */

#include "serialPort.h"

void log_msg(char* msg){
	serialPort_writeString("# ");
	serialPort_writeLine(msg);
}
