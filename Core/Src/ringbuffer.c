/*
 * ringbuffer.c
 *
 *  Created on: Nov 21, 2021
 *      Author: sten
 */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "ringbuffer.h"

 #define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

static inline size_t getFreeSize(ringbuffer_t* ringBuffer){
	if (ringBuffer->wrPtr >= ringBuffer->rdPtr){
		return (ringBuffer->rdPtr - ringBuffer->base) + (ringBuffer->top - ringBuffer->wrPtr);
	}
	else{
		return ringBuffer->rdPtr - ringBuffer->wrPtr - 1;
	}
}

static inline void movePtr(ringbuffer_t* ringBuffer, volatile uint8_t** ptr, size_t offset){
	if (*ptr + offset > ringBuffer->top){
		*ptr = (*ptr + offset) - (ringBuffer->top - ringBuffer->base + 1);
	}
	else{
		*ptr += offset;
	}
}

void ringbuffer_init(ringbuffer_t* ringBuffer, uint8_t* buffer,  size_t size){
	ringBuffer->rdPtr = ringBuffer->wrPtr = ringBuffer->base = buffer;
	ringBuffer->top = buffer + size - 1;
}

int ringbuffer_write(ringbuffer_t* ringBuffer, uint8_t* dataSrc, size_t len){
	size_t bytesTowrite = 0;
	size_t freeSize = getFreeSize(ringBuffer);

	if (len > freeSize){
		return -1;
	}

	if (ringBuffer->rdPtr >= ringBuffer->wrPtr){
		memcpy((void*)ringBuffer->wrPtr, dataSrc, len);
		movePtr(ringBuffer, &ringBuffer->wrPtr, len);
	}
	else{
		size_t bytesAvailableTop = ringBuffer->top - ringBuffer->wrPtr + 1;
		size_t bytesToWriteTop = MIN(bytesAvailableTop, len);

		memcpy((void*)ringBuffer->wrPtr, dataSrc, bytesToWriteTop);

		size_t bytesAvailableBottom = ringBuffer->rdPtr - ringBuffer->base;
		size_t bytesToWriteBottom = MIN(bytesAvailableBottom, len - bytesToWriteTop);
		if (bytesToWriteBottom > 0){
			memcpy(ringBuffer->base, dataSrc + bytesToWriteTop, bytesToWriteBottom);
		}

		bytesTowrite = bytesToWriteTop + bytesToWriteBottom;
	}

	movePtr(ringBuffer, &ringBuffer->wrPtr, bytesTowrite);

	return bytesTowrite;

}

int ringbuffer_writeByte(ringbuffer_t* ringBuffer, uint8_t byte){
	if (getFreeSize(ringBuffer) < 1){
		return -1;
	}

	*(ringBuffer->wrPtr) = byte;
	movePtr(ringBuffer, &ringBuffer->wrPtr, 1);

	return 0;
}

int ringbuffer_read(ringbuffer_t* ringBuffer, uint8_t* dataDest, size_t maxLen){
	size_t bytesToRead = 0;

	if (ringBuffer->wrPtr > ringBuffer->rdPtr){
		size_t bytesAvailable = ringBuffer->wrPtr - ringBuffer->rdPtr;
		bytesToRead = MIN(bytesAvailable, maxLen);

		memcpy(dataDest, (uint8_t*)ringBuffer->rdPtr, bytesToRead);
	}
	else if(ringBuffer->rdPtr > ringBuffer->wrPtr){
		size_t bytesAvailableTop = ringBuffer->top - ringBuffer->rdPtr + 1;
		size_t bytesToReadTop = MIN(bytesAvailableTop, maxLen);

		memcpy(dataDest, (uint8_t*)ringBuffer->rdPtr, bytesToReadTop);

		size_t bytesAvailableBottom = ringBuffer->wrPtr - ringBuffer->base;
		size_t bytesToReadBottom = MIN(bytesAvailableBottom, maxLen - bytesToReadTop);
		if (bytesToReadBottom > 0){
			memcpy(dataDest + bytesToReadTop, ringBuffer->base, bytesToReadBottom);
		}

		bytesToRead = bytesToReadTop + bytesToReadBottom;
	}
	else{
		return 0;
	}

	movePtr(ringBuffer, &ringBuffer->rdPtr, bytesToRead);

	return bytesToRead;
}
