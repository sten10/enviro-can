/*
 * serialPort.c
 *
 *  Created on: Nov 14, 2021
 *      Author: sten
 */

#include <string.h>

#include "serialPort.h"
#include "ringbuffer.h"

static UART_HandleTypeDef* uartHandle;

static uint8_t buffer[64];
static uint8_t readBufer[1];
static ringbuffer_t ringBuffer;

int serialPort_init(UART_HandleTypeDef* _uartHandle){
	ringbuffer_init(&ringBuffer, buffer, sizeof(buffer));

	uartHandle = _uartHandle;

	HAL_UART_Receive_IT(uartHandle, readBufer, 1);
	return 0;
}

int serialPort_writeString(char* str){
	uint16_t len = strlen(str);

	HAL_UART_Transmit(uartHandle, (uint8_t*)str, len, HAL_MAX_DELAY);

	return 0;
}

int serialPort_writeLine(char* str){
	uint16_t len = serialPort_writeString(str);
	len += serialPort_writeString("\r\n");

	return len;
}

int serialPort_read(uint8_t *dataDest, size_t maxLen){
	return ringbuffer_read(&ringBuffer, dataDest, maxLen);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	if (huart->Instance == USART2)
	{
		/* Receive one byte in interrupt mode */
		HAL_UART_Receive_IT(uartHandle, readBufer, 1);
		ringbuffer_writeByte(&ringBuffer, readBufer[0]);
	}
}
