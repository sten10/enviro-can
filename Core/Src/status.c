/*
 * status.c
 *
 *  Created on: Jan 4, 2022
 *      Author: sten
 */

#include "status.h"
#include "led.h"

static uint32_t statusMask;

void status_setBit(uint8_t statusBit){

	led_set(LED_RED, 1);
	statusMask |= (1UL << statusBit);
}

uint32_t status_get(){
	return statusMask;
}
