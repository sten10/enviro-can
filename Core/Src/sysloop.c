/*
 * sysloop.c
 *
 *  Created on: Nov 14, 2021
 *      Author: sten
 */
#include "stm32f0xx_hal.h"

#include "serialPort.h"
#include "cmdHandler.h"
#include "led.h"

int sysloop_init(){
	return 0;
}

void sysloop_task(){

	HAL_PWR_EnterSLEEPMode(0, PWR_SLEEPENTRY_WFI);

	cmdHandler_task();
	led_task();
}
